#ifndef OPOVERCLASS_H
#define OPOVERCLASS_H
#define OVERLOAD_AS_MEMBER 1
#include <iostream>
using namespace std;

class opOverClass
{
 public :
   opOverClass();
   opOverClass(int a_in, int b_in);
   int get_a();
   int get_b();
   //Overload  +
   opOverClass operator+(opOverClass o_in);
   //overload  ==
   bool operator==(opOverClass o_in);
   //Overload assignment operator
   const opOverClass& operator=(const opOverClass&);
#if OVERLOAD_AS_MEMBER
   opOverClass operator*(opOverClass o_in);
#else
   friend opOverClass operator*(opOverClass o_in1, opOverClass o_in2);
#endif
   //Overload stream extraction
   friend ostream& operator <<(ostream& os, const opOverClass& o_in);
   //Overload stream insertion
   friend istream& operator >>(istream& is, opOverClass& o_in);
   virtual ~opOverClass();


   private:
   int a;
   int b;
   int *c;
};
#endif // OPOVERCLASS_H
